package com.carolynvs.bamboo.plugin.trade_depot;

import java.util.List;

public interface BuildExporter
{
    List<ProjectBean> exportProjects();
    ProjectBean exportProject(String key);
    PlanBean exportPlan(String projectKey, String planKey);
    List<RepositoryBean> exportSharedRepositories();
    RepositoryBean exportRepository(long id);
    List<CredentialsBean> exportSharedCredentials();
    List<VariableBean> exportGlobalVariables();
    VariableBean exportGlobalVariable(String key);
}
