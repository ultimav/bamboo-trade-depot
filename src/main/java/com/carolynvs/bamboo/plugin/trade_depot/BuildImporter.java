package com.carolynvs.bamboo.plugin.trade_depot;

import com.atlassian.bamboo.build.PlanCreationDeniedException;

public interface BuildImporter
{
    ProjectBean importProject(ProjectBean project) throws PlanCreationDeniedException;
    RepositoryBean importSharedRepository(RepositoryBean repository);
    CredentialsBean importSharedCredentials(CredentialsBean credentials);
    VariableBean importGlobalVariable(VariableBean variable);
    void deleteProjects();
    void deleteProject(String key);
    void deletePlan(String projectKey, String planKey);
    void deleteGlobalVariable(String variableKey);
}
