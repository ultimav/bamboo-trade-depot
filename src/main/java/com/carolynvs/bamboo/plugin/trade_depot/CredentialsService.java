package com.carolynvs.bamboo.plugin.trade_depot;

import java.util.List;

public interface CredentialsService
{
    CredentialsBean get(long id);
    List<CredentialsBean> getShared();
    CredentialsBean saveShared(CredentialsBean credentials);
}
