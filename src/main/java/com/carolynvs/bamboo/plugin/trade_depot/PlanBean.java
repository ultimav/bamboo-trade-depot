package com.carolynvs.bamboo.plugin.trade_depot;

import java.util.ArrayList;
import java.util.List;

public class PlanBean
{
    public String Key;
    public String Name;
    public String Description = "";
    public boolean IsEnabled = false;
    public List<StageBean> Stages = new ArrayList<StageBean>();
    public List<RepositoryBean> Repositories = new ArrayList<RepositoryBean>();
    public List<TriggerBean> Triggers = new ArrayList<TriggerBean>();
    public List<NotificationBean> Notifications = new ArrayList<NotificationBean>();
    public List<VariableBean> Variables = new ArrayList<VariableBean>();
}


