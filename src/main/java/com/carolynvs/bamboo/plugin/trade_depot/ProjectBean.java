package com.carolynvs.bamboo.plugin.trade_depot;

import java.util.ArrayList;
import java.util.List;

public class ProjectBean
{
    public String Key;
    public String Name;
    public List<PlanBean> Plans = new ArrayList<PlanBean>();
}
