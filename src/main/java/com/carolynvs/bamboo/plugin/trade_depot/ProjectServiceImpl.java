package com.carolynvs.bamboo.plugin.trade_depot;

import com.atlassian.bamboo.build.PlanCreationDeniedException;
import com.atlassian.bamboo.deletion.DeletionService;
import com.atlassian.bamboo.project.Project;
import com.atlassian.bamboo.project.ProjectManager;

import java.util.ArrayList;
import java.util.List;

public class ProjectServiceImpl implements ProjectService
{
    private final ProjectManager projectManager;
    private final DeletionService deletionService;
    private final PlanServiceImpl planService;

    public ProjectServiceImpl(ProjectManager projectManager, DeletionService deletionService, PlanServiceImpl planService)
    {
        this.projectManager = projectManager;
        this.deletionService = deletionService;
        this.planService = planService;
    }

    @Override
    public ProjectBean get(String key)
    {
        key = key.toUpperCase();

        Project project = projectManager.getProjectByKey(key);
        if(project == null)
            return null;

        return convertProject(project);
    }

    @Override
    public List<ProjectBean> getAll()
    {
        List<ProjectBean> projectBeans = new ArrayList<ProjectBean>();
        for(Project project : projectManager.getSortedProjects())
        {
            projectBeans.add(convertProject(project));
        }

        return projectBeans;
    }

    @Override
    public ProjectBean save(ProjectBean projectBean)
            throws PlanCreationDeniedException
    {
        createOrUpdateProject(projectBean);

        for(PlanBean planBean : projectBean.Plans)
        {
            planService.save(projectBean, planBean);
        }

        return projectBean;
    }

    @Override
    public void delete(String key)
    {
        key = key.toUpperCase();

        Project project = projectManager.getProjectByKey(key);
        deletionService.deleteProject(project);
        deletionService.executeDelayedDeletion();
    }

    @Override
    public void deleteAll()
    {
        for(Project project : projectManager.getProjects())
        {
            deletionService.deleteProject(project);
        }
        deletionService.executeDelayedDeletion();
    }

    private void createOrUpdateProject(ProjectBean projectBean) {
        String key = projectBean.Key;
        String name = projectBean.Name;

        Project project = projectManager.getProjectByKey(key);
        if(project == null)
        {
            project = projectManager.createProject(key, name);
        }
        else
        {
            project.setName(name);
        }
        projectManager.saveProject(project);
    }

    private ProjectBean convertProject(Project project)
    {
        ProjectBean projectBean = new ProjectBean();

        projectBean.Key = project.getKey();
        projectBean.Name = project.getName();
        projectBean.Plans = planService.getPlans(project);

        return projectBean;
    }
}
