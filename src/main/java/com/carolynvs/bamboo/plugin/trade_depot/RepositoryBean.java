package com.carolynvs.bamboo.plugin.trade_depot;

import com.carolynvs.bamboo.plugin.trade_depot.repositories.*;
import org.codehaus.jackson.annotate.JsonSubTypes;
import org.codehaus.jackson.annotate.JsonTypeInfo;

@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "Type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = BitbucketRepositoryBean.class, name = BitbucketRepositoryBean.TYPE),
        @JsonSubTypes.Type(value = GitHubRepositoryBean.class, name = GitHubRepositoryBean.TYPE),
        @JsonSubTypes.Type(value = GitRepositoryBean.class, name = GitRepositoryBean.TYPE),
        @JsonSubTypes.Type(value = SubversionRepositoryBean.class, name = SubversionRepositoryBean.TYPE),
        @JsonSubTypes.Type(value = MercurialRepositoryBean.class, name = MercurialRepositoryBean.TYPE) })
public class RepositoryBean
{
    public boolean IsShared = false;
    public String Name;
    public String Type;
    public String Repository;
    public String UserName;
    public String Password;
    public String Branch;
}