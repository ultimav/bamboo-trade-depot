package com.carolynvs.bamboo.plugin.trade_depot;

import com.atlassian.bamboo.chains.Chain;
import com.atlassian.bamboo.repository.RepositoryData;

import java.util.List;

public interface RepositoryService
{
    RepositoryBean get(long id);
    RepositoryBean getLink(long id);
    List<RepositoryBean> getShared();
    RepositoryData getShared(String name);
    RepositoryBean saveSharedRepository(RepositoryBean repository);

    List<Long> lookupIds(Chain plan, List<RepositoryBean> repositories);
}
