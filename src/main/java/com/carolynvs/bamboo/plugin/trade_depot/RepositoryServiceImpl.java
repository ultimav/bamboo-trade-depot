package com.carolynvs.bamboo.plugin.trade_depot;

import com.atlassian.bamboo.chains.Chain;
import com.atlassian.bamboo.plan.cache.ImmutablePlan;
import com.atlassian.bamboo.repository.*;
import com.atlassian.bamboo.user.BambooAuthenticationContext;
import com.atlassian.bamboo.ww2.actions.build.admin.create.BuildConfiguration;
import com.carolynvs.bamboo.plugin.trade_depot.repositories.RepositoryAdapter;
import com.carolynvs.bamboo.plugin.trade_depot.repositories.RepositoryBuilder;
import com.sun.jersey.api.NotFoundException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class RepositoryServiceImpl implements RepositoryService
{
    private final RepositoryDefinitionManager repositoryDefinitionManager;
    private final RepositoryConfigurationService repositoryConfigurationService;
    private final BambooAuthenticationContext authenticationContext;
    private final RepositoryBuilder repositoryBuilder;

    public RepositoryServiceImpl(RepositoryDefinitionManager repositoryDefinitionManager, RepositoryConfigurationService repositoryConfigurationService,
                                 BambooAuthenticationContext authenticationContext, CredentialsServiceImpl credentialsService)
    {
        this.repositoryDefinitionManager = repositoryDefinitionManager;
        this.repositoryConfigurationService = repositoryConfigurationService;
        this.authenticationContext = authenticationContext;

        this.repositoryBuilder = new RepositoryBuilder(credentialsService);
    }

    @Override
    public RepositoryBean get(long id)
    {
        RepositoryDataEntity repositoryData = repositoryDefinitionManager.getRepositoryDataEntity(id);
        if(repositoryData == null)
            throw new NotFoundException();

        RepositoryData repository = new RepositoryDataImpl(repositoryData);
        return convertRepository(repository);
    }

    @Override
    public RepositoryBean getLink(long id)
    {
        RepositoryDataEntity repositoryData = repositoryDefinitionManager.getRepositoryDataEntity(id);
        if(repositoryData == null)
            throw new NotFoundException();

        RepositoryData repository = new RepositoryDataImpl(repositoryData);
        return repositoryBuilder.getLink(repository);
    }

    @Override
    public List<Long> lookupIds(Chain plan, List<RepositoryBean> repositories)
    {
        List<Long> ids = new ArrayList<Long>(repositories.size());
        HashMap<String, Long> globalRepos = buildNameToIdMap(repositoryDefinitionManager.getGlobalRepositoryDefinitions());
        HashMap<String, Long> localRepos = buildNameToIdMap(repositoryDefinitionManager.getRepositoryDefinitionsForPlan(plan));
        
        for(RepositoryBean repository : repositories)
        {
            Long id;

            if(repository.IsShared)
                id = globalRepos.get(repository.Name);
            else
                id = localRepos.get(repository.Name);

            ids.add(id);
        }

        return ids;
    }

    private HashMap<String, Long> buildNameToIdMap(List<? extends RepositoryData> repositories)
    {
        HashMap<String, Long> map = new HashMap<String, Long>(repositories.size());
        
        for(RepositoryData repository : repositories)
        {
            map.put(repository.getName(), repository.getId());    
        }
        
        return map;
    }
    
    @Override
    public RepositoryData getShared(String name)
    {
        List<RepositoryData> repositories = repositoryDefinitionManager.getGlobalRepositoryDefinitions();
        for(RepositoryData repository : repositories)
        {
            if(repository.getName().equals(name))
                return repository;
        }

        throw new NotFoundException();
    }

    @Override
    public RepositoryBean saveSharedRepository(RepositoryBean repository)
    {
        String webRepoKey = null;
        Boolean triggerBuild = true;
        BuildConfiguration cfg = repositoryBuilder.getConfiguration(repository);

        RepositoryData result = repositoryConfigurationService.createGlobalRepository(repository.Name, repository.Type, webRepoKey, cfg, triggerBuild, authenticationContext.getUser());

        return repository;
    }

    @Override
    public List<RepositoryBean> getShared()
    {
        List<RepositoryBean> repositories = new ArrayList<RepositoryBean>();

        for(RepositoryData repository : repositoryDefinitionManager.getGlobalRepositoryDefinitions())
        {
            repositories.add(convertRepository(repository));
        }

        return repositories;
    }

    public List<RepositoryBean> getRepositories(ImmutablePlan plan)
    {
        List<RepositoryBean> repositories = new ArrayList<RepositoryBean>();

        for(RepositoryDefinition repository : repositoryDefinitionManager.getRepositoryDefinitionsForPlan(plan))
        {
            if(repository.isGlobal())
                repositories.add(convertRepositoryLink(repository));
            else
                repositories.add(convertRepository(repository));
        }

        return repositories;
    }

    private RepositoryBean convertRepository(RepositoryData repository)
    {
        return repositoryBuilder.getDefinition(repository);
    }

    private RepositoryBean convertRepositoryLink(RepositoryData repository)
    {
        return repositoryBuilder.getLink(repository);
    }

    public void saveLocalRepository(RepositoryBean repository, Chain plan)
    {
        String webRepoKey = null;
        Boolean triggerBuild = true;
        BuildConfiguration cfg = repositoryBuilder.getConfiguration(repository);

        repositoryConfigurationService.createRepository(plan, repository.Name, repository.Type, webRepoKey, cfg, triggerBuild);
    }
}
