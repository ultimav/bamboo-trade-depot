package com.carolynvs.bamboo.plugin.trade_depot;

import com.carolynvs.bamboo.plugin.trade_depot.triggers.*;
import org.codehaus.jackson.annotate.JsonSubTypes;
import org.codehaus.jackson.annotate.JsonTypeInfo;

@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "Type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = PollingTriggerBean.class, name = PollingTriggerBean.TYPE) })
public class TriggerBean
{
    public String Type;
    public String Name;
    public String Description;
}
