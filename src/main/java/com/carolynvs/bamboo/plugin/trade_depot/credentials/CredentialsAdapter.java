package com.carolynvs.bamboo.plugin.trade_depot.credentials;

import com.atlassian.bamboo.credentials.CredentialsData;
import com.carolynvs.bamboo.plugin.trade_depot.CredentialsBean;

public abstract class CredentialsAdapter
{
    public abstract CredentialsData getData(CredentialsBean credentials);

    public abstract CredentialsBean getDefinition(CredentialsData credentials);
}

