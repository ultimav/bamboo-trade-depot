package com.carolynvs.bamboo.plugin.trade_depot.credentials;

import com.atlassian.bamboo.credentials.CredentialsData;
import com.atlassian.bamboo.security.EncryptionService;
import com.carolynvs.bamboo.plugin.trade_depot.CredentialsBean;

public class CredentialsBuilder
{
    private final EncryptionService encryptionService;

    public CredentialsBuilder(EncryptionService encryptionService)
    {
        this.encryptionService = encryptionService;
    }

    public CredentialsData build(CredentialsBean credentials)
    {
        CredentialsAdapter adapter = resolveAdapter(credentials);
        return adapter.getData(credentials);
    }

    public CredentialsBean build(CredentialsData credentials)
    {
        CredentialsAdapter adapter = resolveAdapter(credentials);
        return adapter.getDefinition(credentials);
    }

    private CredentialsAdapter resolveAdapter(CredentialsData credentials)
    {
        if(SSHCredentialsAdapter.supports(credentials))
            return new SSHCredentialsAdapter(encryptionService);

        throw new UnsupportedOperationException(String.format("Credentials of type %s is not supported yet.", credentials.getClass().getSimpleName()));
    }

    private CredentialsAdapter resolveAdapter(CredentialsBean credentials)
    {
        if(SSHCredentialsAdapter.supports(credentials))
            return new SSHCredentialsAdapter(encryptionService);

        throw new UnsupportedOperationException(String.format("Credentials of type %s is not supported yet.", credentials.Type));
    }
}
