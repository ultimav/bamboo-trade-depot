package com.carolynvs.bamboo.plugin.trade_depot.credentials;

import com.atlassian.bamboo.credentials.CredentialsData;
import com.atlassian.bamboo.credentials.PrivateKeyCredentials;
import com.atlassian.bamboo.credentials.SshCredentialsImpl;
import com.atlassian.bamboo.security.EncryptionService;
import com.carolynvs.bamboo.plugin.trade_depot.CredentialsBean;

public class SSHCredentialsAdapter extends CredentialsAdapter
{
    private final EncryptionService encryptionService;

    public static boolean supports(CredentialsBean credentials)
    {
        return SSHCredentialsBean.class.isInstance(credentials);
    }

    public static boolean supports(CredentialsData credentials)
    {
        return true;
    }

    public SSHCredentialsAdapter(EncryptionService encryptionService)
    {
        this.encryptionService = encryptionService;
    }

    @Override
    public CredentialsData getData(CredentialsBean credentials)
    {
        return getCredentialsData((SSHCredentialsBean)credentials);
    }

    private CredentialsData getCredentialsData(SSHCredentialsBean credentials) {
        PrivateKeyCredentials privateKeyCredentials = new SshCredentialsImpl(credentials.PrivateKey, credentials.Passphrase);
        return privateKeyCredentials.convertToCredentials(encryptionService, credentials.Name);
    }

    @Override
    public CredentialsBean getDefinition(CredentialsData credentialsData)
    {
        SSHCredentialsBean sshCredentials = new SSHCredentialsBean();
        sshCredentials.Name = credentialsData.getName();
        return sshCredentials;
    }
}
