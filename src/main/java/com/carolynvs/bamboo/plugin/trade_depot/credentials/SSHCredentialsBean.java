package com.carolynvs.bamboo.plugin.trade_depot.credentials;

import com.carolynvs.bamboo.plugin.trade_depot.CredentialsBean;

public class SSHCredentialsBean extends CredentialsBean
{
    public static final String TYPE = "ssh";

    public SSHCredentialsBean()
    {
        this.Type = TYPE;
    }

    public String PrivateKey;
    public String Passphrase;
}
