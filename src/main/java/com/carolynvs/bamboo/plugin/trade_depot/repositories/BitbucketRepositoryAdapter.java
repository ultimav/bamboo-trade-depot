package com.carolynvs.bamboo.plugin.trade_depot.repositories;

import com.atlassian.bamboo.plugins.hg.BitbucketRepository;
import com.carolynvs.bamboo.plugin.trade_depot.RepositoryBean;
import org.apache.commons.configuration.HierarchicalConfiguration;

public class BitbucketRepositoryAdapter extends RepositoryAdapter
{
    public static boolean supports(String repositoryType)
    {
        return repositoryType.equals(BitbucketRepositoryBean.TYPE);
    }

    public static class ConfigKeys
    {
        public static final String UserName = BitbucketRepository.REPOSITORY_BITBUCKET_USERNAME;
        public static final String Password = BitbucketRepository.REPOSITORY_BITBUCKET_PASSWORD;
        public static final String Repository = BitbucketRepository.REPOSITORY_BITBUCKET_REPOSITORY;
        public static final String Branch = BitbucketRepository.REPOSITORY_BITBUCKET_BRANCH;
        public static final String SCM = BitbucketRepository.REPOSITORY_BITBUCKET_SCM;
    }

    @Override
    protected String getUserNameKey()
    {
        return ConfigKeys.UserName;
    }

    @Override
    protected String getPasswordKey()
    {
        return ConfigKeys.Password;
    }

    @Override
    protected String getRepositoryKey()
    {
        return ConfigKeys.Repository;
    }

    @Override
    protected RepositoryBean createRepositoryBean()
    {
        return new BitbucketRepositoryBean();
    }

    @Override
    public void readConfiguration(HierarchicalConfiguration cfg, RepositoryBean repository)
    {
        super.readConfiguration(cfg, repository);

        BitbucketRepositoryBean bitBucketRepository = (BitbucketRepositoryBean)repository;
        bitBucketRepository.Branch = cfg.getString(ConfigKeys.Branch);
        bitBucketRepository.SCM = cfg.getString(ConfigKeys.SCM);
    }

    @Override
    protected void writeConfiguration(RepositoryBean repository, HierarchicalConfiguration cfg)
    {
        super.writeConfiguration(repository, cfg);

        BitbucketRepositoryBean bitBucketRepository = (BitbucketRepositoryBean)repository;
        cfg.setProperty(ConfigKeys.Branch, bitBucketRepository.Branch);
        cfg.setProperty(ConfigKeys.SCM, bitBucketRepository.SCM);
    }
}

