package com.carolynvs.bamboo.plugin.trade_depot.repositories;

import com.carolynvs.bamboo.plugin.trade_depot.RepositoryBean;

public class BitbucketRepositoryBean extends RepositoryBean
{
    public static final String TYPE = "com.atlassian.bamboo.plugins.atlassian-bamboo-plugin-mercurial:bb";

    public BitbucketRepositoryBean()
    {
        this.Type = TYPE;
    }

    public String SCM;
}
