package com.carolynvs.bamboo.plugin.trade_depot.repositories;

import com.carolynvs.bamboo.plugin.trade_depot.RepositoryBean;
import org.apache.commons.configuration.HierarchicalConfiguration;

public class GitHubRepositoryAdapter extends RepositoryAdapter
{
    public static boolean supports(String repositoryType)
    {
        return repositoryType.equals(GitHubRepositoryBean.TYPE);
    }

    public static class ConfigKeys
    {

        public static final String UserName = "repository.github.username";
        public static final String Password = "repository.github.password";
        public static final String Repository = "repository.github.repository";
        public static final String Branch = "repository.github.branch";
        public static final String UseShallowClones = "repository.github.useShallowClones";
        public static final String UseSubmodules = "repository.github.useSubmodules";
        public static final String CommandTimeout = "repository.github.commandTimeout";
        public static final String VerboseLogs = "repository.github.verbose.logs";
        public static final String FetchWholeRepository = "repository.github.fetch.whole.repository";
    }


    @Override
    protected String getUserNameKey()
    {
        return ConfigKeys.UserName;
    }

    @Override
    protected String getPasswordKey()
    {
        return ConfigKeys.Password;
    }

    @Override
    protected String getRepositoryKey()
    {
        return ConfigKeys.Repository;
    }

    @Override
    protected RepositoryBean createRepositoryBean()
    {
        return new GitHubRepositoryBean();
    }

    @Override
    protected void readConfiguration(HierarchicalConfiguration cfg, RepositoryBean repository)
    {
        super.readConfiguration(cfg, repository);
        repository.Branch = cfg.getString(ConfigKeys.Branch);
    }

    @Override
    protected void writeConfiguration(RepositoryBean repository, HierarchicalConfiguration cfg)
    {
        super.writeConfiguration(repository, cfg);
        cfg.setProperty(ConfigKeys.Branch, repository.Branch);
    }
}
