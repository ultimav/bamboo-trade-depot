package com.carolynvs.bamboo.plugin.trade_depot.repositories;

import com.carolynvs.bamboo.plugin.trade_depot.RepositoryBean;

public class GitHubRepositoryBean extends RepositoryBean
{
    public static final String TYPE = "com.atlassian.bamboo.plugins.atlassian-bamboo-plugin-git:gh";

    public GitHubRepositoryBean()
    {
        this.Type = TYPE;
    }
}

