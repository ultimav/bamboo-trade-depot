package com.carolynvs.bamboo.plugin.trade_depot.repositories;

import com.carolynvs.bamboo.plugin.trade_depot.*;
import org.apache.commons.configuration.HierarchicalConfiguration;

public class GitRepositoryAdapter extends RepositoryAdapter
{
    public static boolean supports(String repositoryType)
    {
        return repositoryType.equals(GitRepositoryBean.TYPE);
    }

    private final CredentialsServiceImpl credentialsService;

    public GitRepositoryAdapter(CredentialsServiceImpl credentialsService)
    {
        this.credentialsService = credentialsService;
    }

    public static class ConfigKeys
    {

        public static final String Repository = "repository.git.repositoryUrl";
        public static final String AuthenticationType = "repository.git.authenticationType";
        public static final String UserName = "repository.git.username";
        public static final String Password = "repository.git.password";
        public static final String Branch = "repository.git.branch";
        public static final String PrivateKey = "repository.git.ssh.key";
        public static final String Passphrase = "repository.git.ssh.passphrase";
        public static final String UseShallowClones = "repository.git.useShallowClones";
        public static final String UseAgentCache = "repository.git.useRemoteAgentCache";
        public static final String UseSubmodules = "repository.git.useSubmodules";
        public static final String CommandTimeout = "repository.git.commandTimeout";
        public static final String VerboseLogs = "repository.git.verbose.logs";
        public static final String FetchWholeRepository = "repository.git.fetch.whole.repository";
        public static final String SharedCredentialsId = "repository.git.sharedCrendentials";
    }

    @Override
    protected String getUserNameKey()
    {
        return ConfigKeys.UserName;
    }

    @Override
    protected String getPasswordKey()
    {
        return ConfigKeys.Password;
    }

    @Override
    protected String getRepositoryKey()
    {
        return ConfigKeys.Repository;
    }

    @Override
    protected RepositoryBean createRepositoryBean()
    {
        return new GitRepositoryBean();
    }

    @Override
    protected void readConfiguration(HierarchicalConfiguration cfg, RepositoryBean repository)
    {
        super.readConfiguration(cfg, repository);

        GitRepositoryBean gitRepo = (GitRepositoryBean)repository;
        gitRepo.Branch = cfg.getString(ConfigKeys.Branch);
        gitRepo.AuthenticationType = cfg.getString(ConfigKeys.AuthenticationType);
        gitRepo.SharedCredentialsName = getSharedCredentialsName(cfg.getLong(ConfigKeys.SharedCredentialsId));
    }

    @Override
    protected void writeConfiguration(RepositoryBean repository, HierarchicalConfiguration cfg)
    {
        super.writeConfiguration(repository, cfg);

        GitRepositoryBean gitRepo = (GitRepositoryBean)repository;
        cfg.setProperty(ConfigKeys.Branch, gitRepo.Branch);
        cfg.setProperty(ConfigKeys.AuthenticationType, gitRepo.AuthenticationType);
        cfg.setProperty(ConfigKeys.PrivateKey, gitRepo.PrivateKey);
        cfg.setProperty(ConfigKeys.Passphrase, gitRepo.Passphrase);
        cfg.setProperty(ConfigKeys.SharedCredentialsId, getSharedCredentialsId(gitRepo.SharedCredentialsName));
    }

    private long getSharedCredentialsId(String name)
    {
        return credentialsService.findSharedByName(name).getId();
    }

    private String getSharedCredentialsName(long id)
    {
        return credentialsService.get(id).Name;
    }
}

