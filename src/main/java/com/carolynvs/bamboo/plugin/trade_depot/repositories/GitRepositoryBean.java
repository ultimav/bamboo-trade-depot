package com.carolynvs.bamboo.plugin.trade_depot.repositories;

import com.carolynvs.bamboo.plugin.trade_depot.RepositoryBean;

public class GitRepositoryBean extends RepositoryBean
{
    public static final String TYPE = "com.atlassian.bamboo.plugins.atlassian-bamboo-plugin-git:git";

    public GitRepositoryBean()
    {
        this.Type = TYPE;
    }

    public String AuthenticationType;
    public String PrivateKey;
    public String Passphrase;
    public String SharedCredentialsName;
}