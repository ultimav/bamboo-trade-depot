package com.carolynvs.bamboo.plugin.trade_depot.repositories;

import com.atlassian.bamboo.plugins.hg.HgRepository;
import com.carolynvs.bamboo.plugin.trade_depot.RepositoryBean;
import org.apache.commons.configuration.HierarchicalConfiguration;

public class MercurialRepositoryAdapter extends RepositoryAdapter
{
    public static boolean supports(String type)
    {
        return MercurialRepositoryBean.TYPE.equals(type);
    }

    public static class ConfigKeys
    {
        public static final String Repository = HgRepository.REPOSITORY_HG_REPOSITORY_URL;
        public static final String AuthenticationType = HgRepository.REPOSITORY_HG_AUTHENTICATION;
        public static final String UserName = HgRepository.REPOSITORY_HG_USERNAME;
        public static final String Password = HgRepository.REPOSITORY_HG_PASSWORD;
        public static final String PrivateKey = "repository.hg.ssh.keyFromFile";
        public static final String Passphrase = "repository.hg.ssh.passphrase";
    }

    @Override
    protected String getUserNameKey()
    {
        return ConfigKeys.UserName;
    }

    @Override
    protected String getPasswordKey()
    {
        return ConfigKeys.Password;
    }

    @Override
    protected String getRepositoryKey()
    {
        return ConfigKeys.Repository;
    }

    @Override
    protected RepositoryBean createRepositoryBean()
    {
        return new MercurialRepositoryBean();
    }

    @Override
    protected void readConfiguration(HierarchicalConfiguration cfg, RepositoryBean repository)
    {
        super.readConfiguration(cfg, repository);

        MercurialRepositoryBean hgRepo = (MercurialRepositoryBean)repository;
        hgRepo.AuthenticationType = cfg.getString(ConfigKeys.AuthenticationType);
    }

    @Override
    protected void writeConfiguration(RepositoryBean repository, HierarchicalConfiguration cfg)
    {
        super.writeConfiguration(repository, cfg);

        MercurialRepositoryBean hgRepo = (MercurialRepositoryBean)repository;
        cfg.setProperty(ConfigKeys.AuthenticationType, hgRepo.AuthenticationType);
        cfg.setProperty(ConfigKeys.PrivateKey, hgRepo.PrivateKey);
        cfg.setProperty(ConfigKeys.Passphrase, hgRepo.Passphrase);
    }
}

