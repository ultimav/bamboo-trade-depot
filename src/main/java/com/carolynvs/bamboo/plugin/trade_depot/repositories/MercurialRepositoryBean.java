package com.carolynvs.bamboo.plugin.trade_depot.repositories;

import com.carolynvs.bamboo.plugin.trade_depot.RepositoryBean;

public class MercurialRepositoryBean extends RepositoryBean
{
    public static final String TYPE = "com.atlassian.bamboo.plugins.atlassian-bamboo-plugin-mercurial:hg";

    public MercurialRepositoryBean()
    {
        this.Type = TYPE;
    }

    public String AuthenticationType;
    public String PrivateKey;
    public String Passphrase;
}
