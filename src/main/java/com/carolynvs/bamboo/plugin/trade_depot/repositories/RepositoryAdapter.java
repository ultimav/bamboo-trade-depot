package com.carolynvs.bamboo.plugin.trade_depot.repositories;

import com.atlassian.bamboo.repository.RepositoryData;
import com.atlassian.bamboo.ww2.actions.build.admin.create.BuildConfiguration;
import com.carolynvs.bamboo.plugin.trade_depot.RepositoryBean;
import org.apache.commons.configuration.HierarchicalConfiguration;

public abstract class RepositoryAdapter
{
    protected abstract String getUserNameKey();
    protected abstract String getPasswordKey();
    protected abstract String getRepositoryKey();

    protected abstract RepositoryBean createRepositoryBean();

    protected void readConfiguration(HierarchicalConfiguration cfg, RepositoryBean repository)
    {
        repository.Repository = cfg.getString(getRepositoryKey());
        repository.UserName = cfg.getString(getUserNameKey());

        // purposefully not including authentication data like a keypair or password
    }

    protected void writeConfiguration(RepositoryBean repository, HierarchicalConfiguration cfg)
    {
        cfg.setProperty(getRepositoryKey(), repository.Repository);
        cfg.setProperty(getUserNameKey(), repository.UserName);
        cfg.setProperty(getPasswordKey(), repository.Password);
    }

    public final RepositoryBean getLink(RepositoryData repository)
    {
        RepositoryBean bean = createRepositoryBean();

        bean.Name = repository.getName();
        bean.IsShared = repository.isGlobal();

        return bean;
    }

    public final RepositoryBean getDefinition(RepositoryData repository)
    {
        RepositoryBean bean = createRepositoryBean();

        bean.Name = repository.getName();
        bean.IsShared = repository.isGlobal();

        readConfiguration(repository.getConfiguration(), bean);

        return bean;
    }

    public final BuildConfiguration getConfiguration(RepositoryBean repository)
    {
        BuildConfiguration cfg = new BuildConfiguration();
        writeConfiguration(repository, cfg);
        return cfg;
    }
}
