package com.carolynvs.bamboo.plugin.trade_depot.repositories;

import com.atlassian.bamboo.repository.RepositoryData;
import com.atlassian.bamboo.ww2.actions.build.admin.create.BuildConfiguration;
import com.carolynvs.bamboo.plugin.trade_depot.*;

public class RepositoryBuilder
{
    private final CredentialsServiceImpl credentialsService;

    public RepositoryBuilder(CredentialsServiceImpl credentialsService)
    {
        this.credentialsService = credentialsService;
    }

    public final RepositoryBean getLink(RepositoryData repository)
    {
        RepositoryAdapter adapter = resolveAdapter(repository);
        return adapter.getLink(repository);
    }

    public final RepositoryBean getDefinition(RepositoryData repository)
    {
        RepositoryAdapter adapter = resolveAdapter(repository);
        return adapter.getDefinition(repository);
    }

    public final BuildConfiguration getConfiguration(RepositoryBean repository)
    {
        RepositoryAdapter adapter = resolveAdapter(repository);
        return adapter.getConfiguration(repository);
    }

    private RepositoryAdapter resolveAdapter(RepositoryData repository)
    {
        RepositoryAdapter adapter = resolveAdapter(repository.getPluginKey());

        return adapter;
    }

    private RepositoryAdapter resolveAdapter(RepositoryBean repository)
    {
        return resolveAdapter(repository.Type);
    }

    private RepositoryAdapter resolveAdapter(String type)
    {
        // todo: use spring to register these by their plugin key and resolveAdapter dynamically
        if(BitbucketRepositoryAdapter.supports(type))
            return new BitbucketRepositoryAdapter();

        if(GitHubRepositoryAdapter.supports(type))
            return new GitHubRepositoryAdapter();

        if(GitRepositoryAdapter.supports(type))
            return new GitRepositoryAdapter(credentialsService);

        if(SubversionRepositoryAdapter.supports(type))
            return new SubversionRepositoryAdapter();

        if(MercurialRepositoryAdapter.supports(type))
            return new MercurialRepositoryAdapter();

        throw new UnsupportedOperationException(String.format("Repository of type %s is not supported yet.", type));
    }
}
