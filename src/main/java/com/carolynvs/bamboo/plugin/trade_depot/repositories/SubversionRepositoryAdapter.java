package com.carolynvs.bamboo.plugin.trade_depot.repositories;

import com.atlassian.bamboo.repository.AuthenticationType;
import com.carolynvs.bamboo.plugin.trade_depot.RepositoryBean;
import org.apache.commons.configuration.HierarchicalConfiguration;

public class SubversionRepositoryAdapter extends RepositoryAdapter
{
    public static boolean supports(String type)
    {
        return SubversionRepositoryBean.TYPE.equals(type);
    }

    public static class ConfigKeys
    {
        public static final String Repository = "repository.svn.repositoryUrl";
        public static final String UserName = "repository.svn.username";
        public static final String AuthenticationType = "repository.svn.authType";
        public static final String Password = "repository.svn.userPassword";
        public static final String SSH_PrivateKeyFile = "repository.svn.keyFile";
        public static final String SSH_Passphrase = "repository.svn.passphrase";
        public static final String SSL_PrivateKeyFile = "repository.svn.sslKeyFile";
        public static final String SSL_Passphrase = "repository.svn.sslPassphrase";
        public static final String UseExternals = "repository.svn.useExternals";
        public static final String UseExport = "repository.svn.useExport";
        public static final String BranchAutodetectRoot = "repository.svn.branch.autodetectRootUrl";
        public static final String BranchManualRoot = "repository.svn.branch.manualRootUrl";
        public static final String TagAutoDetectRoot = "repository.svn.tag.autodetectRootUrl";
        public static final String TagManualRoot = "repository.svn.tag.manualRootUrl";
    }

    @Override
    protected String getUserNameKey()
    {
        return ConfigKeys.UserName;
    }

    @Override
    protected String getPasswordKey()
    {
        return ConfigKeys.Password;
    }

    @Override
    protected String getRepositoryKey()
    {
        return ConfigKeys.Repository;
    }

    @Override
    protected RepositoryBean createRepositoryBean()
    {
        return new SubversionRepositoryBean();
    }

    @Override
    protected void readConfiguration(HierarchicalConfiguration cfg, RepositoryBean repository)
    {
        super.readConfiguration(cfg, repository);

        SubversionRepositoryBean svnRepository = (SubversionRepositoryBean)repository;
        svnRepository.AuthenticationType = cfg.getString(ConfigKeys.AuthenticationType);
    }

    @Override
    protected void writeConfiguration(RepositoryBean repository, HierarchicalConfiguration cfg)
    {
        super.writeConfiguration(repository, cfg);

        SubversionRepositoryBean svnRepository = (SubversionRepositoryBean)repository;
        cfg.setProperty(ConfigKeys.AuthenticationType, svnRepository.AuthenticationType);

        if(hasAuthType(svnRepository, AuthenticationType.SSH))
        {
            cfg.setProperty(ConfigKeys.SSH_PrivateKeyFile, svnRepository.PrivateKey);
            cfg.setProperty(ConfigKeys.SSH_Passphrase, svnRepository.Passphrase);
        }
        else if(hasAuthType(svnRepository, AuthenticationType.SSL_CLIENT_CERTIFICATE))
        {
            cfg.setProperty(ConfigKeys.SSL_PrivateKeyFile, svnRepository.PrivateKey);
            cfg.setProperty(ConfigKeys.SSL_Passphrase, svnRepository.Passphrase);
        }
    }

    private boolean hasAuthType(SubversionRepositoryBean repository, AuthenticationType authType)
    {
        return repository.AuthenticationType.equals(authType.getKey());
    }
}

