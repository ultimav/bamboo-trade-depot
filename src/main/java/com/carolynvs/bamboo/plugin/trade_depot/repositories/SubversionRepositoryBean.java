package com.carolynvs.bamboo.plugin.trade_depot.repositories;

import com.carolynvs.bamboo.plugin.trade_depot.RepositoryBean;

public class SubversionRepositoryBean extends RepositoryBean
{
    public static final String TYPE = "com.atlassian.bamboo.plugin.system.repository:svn";

    public SubversionRepositoryBean()
    {
        this.Type = TYPE;
    }

    public String AuthenticationType;
    public String PrivateKey;
    public String Passphrase;
}
