package com.carolynvs.bamboo.plugin.trade_depot.rest;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public class BadRequestException extends WebApplicationException
{
    public BadRequestException(String error)
    {
        super(Response.status(Response.Status.BAD_REQUEST).type(MediaType.APPLICATION_JSON)
                .entity(error).build());
    }
}

