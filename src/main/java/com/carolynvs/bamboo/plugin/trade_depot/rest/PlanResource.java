package com.carolynvs.bamboo.plugin.trade_depot.rest;

import com.carolynvs.bamboo.plugin.trade_depot.BuildExporter;
import com.carolynvs.bamboo.plugin.trade_depot.BuildImporter;
import com.carolynvs.bamboo.plugin.trade_depot.PlanBean;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

public class PlanResource
{
    private final String projectKey;
    private final BuildExporter exporter;
    private final BuildImporter importer;

    public PlanResource(String projectKey, BuildExporter exporter, BuildImporter importer)
    {
        this.projectKey = projectKey;
        this.exporter = exporter;
        this.importer = importer;
    }

    @GET
    public Response get()
    {
        List<PlanBean> plans = exporter.exportProject(projectKey).Plans;
        return Response.ok(plans).build();
    }

    @GET
    @Path("{plan-key}")
    public Response retrieve(@PathParam("plan-key") String planKey)
    {
        PlanBean plan = exporter.exportPlan(projectKey, planKey);
        if(plan == null)
            return Response.status(Response.Status.NOT_FOUND).build();

        return Response.ok(plan).build();
    }

    @DELETE
    @Path("{plan-key}")
    public Response delete(@PathParam("plan-key") String planKey)
    {
        importer.deletePlan(projectKey, planKey);

        return Response.noContent().build();
    }
}
