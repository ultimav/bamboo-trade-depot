package com.carolynvs.bamboo.plugin.trade_depot.triggers;

import com.atlassian.bamboo.build.strategy.BuildStrategy;
import com.atlassian.bamboo.build.strategy.PollingBuildStrategy;
import com.carolynvs.bamboo.plugin.trade_depot.RepositoryBean;
import com.carolynvs.bamboo.plugin.trade_depot.RepositoryService;
import com.carolynvs.bamboo.plugin.trade_depot.RepositoryServiceImpl;
import com.carolynvs.bamboo.plugin.trade_depot.TriggerBean;
import org.apache.commons.configuration.HierarchicalConfiguration;
import org.apache.commons.lang.StringUtils;

import java.util.*;

public class PollingTriggerAdapter extends TriggerAdapter
{
    public static boolean supports(String type)
    {
        return PollingTriggerBean.TYPE.equals(type);
    }

    public static class ConfigKeys
    {
        public static final String ShouldRequirePassingPlans = "custom.triggerrCondition.plansGreen.enabled";
        public static final String RequiredPassingPlanKeys = "custom.triggerrCondition.plansGreen.plans";
        public static final String CronExpression = "repository.change.poll.cronExpression";
        public static final String PollingFrequency = "repository.change.poll.pollingPeriod";
        public static final String PollingStrategy = "repository.change.poll.type";
    }

    private static final String DefaultCronExpression = "0 0 0 ? * *";

    private final RepositoryService repositoryService;

    public PollingTriggerAdapter(RepositoryService repositoryService)
    {
        this.repositoryService = repositoryService;
    }

    @Override
    protected void readConfiguration(BuildStrategy trigger, TriggerBean triggerBean)
    {
        PollingBuildStrategy pollingTrigger = (PollingBuildStrategy)trigger;
        PollingTriggerBean pollingTriggerBean = (PollingTriggerBean)triggerBean;

        pollingTriggerBean.PollingStrategy = pollingTrigger.getPollingStrategy();
        pollingTriggerBean.PollingFrequency = pollingTrigger.getPollingPeriod();
        pollingTriggerBean.Repositories = getRepositoryLinks(pollingTrigger);
        pollingTriggerBean.RequiredPassingPlanKeys = getRequirePassingPlansBeforeBuild(pollingTrigger);
    }

    @Override
    protected void writeConfiguration(TriggerBean trigger, TriggerData triggerData)
    {
        super.writeConfiguration(trigger, triggerData);

        PollingTriggerBean pollingTriggerBean = (PollingTriggerBean)trigger;

        triggerData.Repositories = getRepositoryIds(pollingTriggerBean);
        triggerData.Configuration.setProperty(ConfigKeys.PollingStrategy, pollingTriggerBean.PollingStrategy);
        triggerData.Configuration.setProperty(ConfigKeys.PollingFrequency, pollingTriggerBean.PollingFrequency);
        triggerData.Configuration.setProperty(ConfigKeys.CronExpression, DefaultCronExpression);

        String requiredPassingPlans = buildRequiredPassingPlans(pollingTriggerBean);
        boolean shouldRequirePassingPlans = requiredPassingPlans != null;
        triggerData.Configuration.setProperty(ConfigKeys.ShouldRequirePassingPlans, shouldRequirePassingPlans);
        triggerData.Configuration.setProperty(ConfigKeys.RequiredPassingPlanKeys, requiredPassingPlans);
    }

    private String buildRequiredPassingPlans(PollingTriggerBean triggerBean)
    {
        return StringUtils.join(triggerBean.RequiredPassingPlanKeys, ';');
    }

    private HashSet<Long> getRepositoryIds(PollingTriggerBean trigger)
    {
        List<Long> ids = repositoryService.lookupIds(plan, trigger.Repositories);
        return new HashSet<Long>(ids);
    }

    @Override
    protected TriggerBean createTriggerBean()
    {
        return new PollingTriggerBean();
    }

    private List<RepositoryBean> getRepositoryLinks(PollingBuildStrategy trigger)
    {
        List<RepositoryBean> repositories = new ArrayList<RepositoryBean>();

         for(long repositoryId : trigger.getTriggeringRepositories())
         {
             repositories.add(repositoryService.getLink(repositoryId));
         }

        return repositories;
    }

    private List<String> getRequirePassingPlansBeforeBuild(PollingBuildStrategy trigger)
    {
        Map<String,String> cfg = trigger.getTriggerConditionsConfiguration();
        String planKeys = cfg.get(ConfigKeys.RequiredPassingPlanKeys);

        return Arrays.asList(StringUtils.split(planKeys, ';'));
    }
}

