package com.carolynvs.bamboo.plugin.trade_depot.triggers;

import com.atlassian.bamboo.trigger.TriggerTypeManager;
import com.carolynvs.bamboo.plugin.trade_depot.RepositoryBean;
import com.carolynvs.bamboo.plugin.trade_depot.TriggerBean;

import java.util.List;

public class PollingTriggerBean extends TriggerBean
{
    public static final String TYPE = TriggerTypeManager.BUILD_STRATEGY_POLLING;

    public PollingTriggerBean()
    {
        this.Type = TYPE;
    }

    public String PollingStrategy;
    public int PollingFrequency;
    public List<RepositoryBean> Repositories;
    public List<String> RequiredPassingPlanKeys;
}
