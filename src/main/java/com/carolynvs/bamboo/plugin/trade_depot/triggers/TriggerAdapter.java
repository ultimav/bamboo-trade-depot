package com.carolynvs.bamboo.plugin.trade_depot.triggers;

import com.atlassian.bamboo.build.strategy.BuildStrategy;
import com.atlassian.bamboo.chains.Chain;
import com.carolynvs.bamboo.plugin.trade_depot.TriggerBean;
import org.apache.commons.configuration.HierarchicalConfiguration;

import java.util.HashSet;

public abstract class TriggerAdapter
{
    private static class ConfigKeys
    {
        public static final String BuildStrategy = "selectedBuildStrategy";
    }

    protected Chain plan;

    public final TriggerBean getDefinition(BuildStrategy trigger)
    {
        TriggerBean triggerBean = createTriggerBean();

        triggerBean.Name = trigger.getName();
        triggerBean.Description = trigger.getUserDescription();

        readConfiguration(trigger, triggerBean);
        return triggerBean;
    }

    protected abstract TriggerBean createTriggerBean();
    protected abstract void readConfiguration(BuildStrategy trigger, TriggerBean triggerBean);

    protected void writeConfiguration(TriggerBean triggerBean, TriggerData triggerData)
    {
        triggerData.Plan = plan.getPlanKey();
        triggerData.Description = triggerBean.Description;

        triggerData.Configuration.setProperty(ConfigKeys.BuildStrategy, triggerBean.Type);
    }

    protected void initialize(Chain plan)
    {
        this.plan = plan;
    }

    public final TriggerData getData(Chain plan, TriggerBean trigger)
    {
        initialize(plan);

        TriggerData data = new TriggerData();

        writeConfiguration(trigger, data);

        return data;
    }


}
