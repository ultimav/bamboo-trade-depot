package com.carolynvs.bamboo.plugin.trade_depot.triggers;

import com.atlassian.bamboo.build.strategy.BuildStrategy;
import com.atlassian.bamboo.chains.Chain;
import com.atlassian.bamboo.plan.cache.ImmutableTopLevelPlan;
import com.atlassian.bamboo.trigger.TriggerTypeManager;
import com.carolynvs.bamboo.plugin.trade_depot.*;
import org.apache.commons.configuration.HierarchicalConfiguration;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class TriggerBuilder
{
    private final RepositoryService repositoryService;

    public TriggerBuilder(RepositoryService repositoryService)
    {
        this.repositoryService = repositoryService;
    }

    public List<TriggerBean> build(ImmutableTopLevelPlan plan)
    {
        List<TriggerBean> triggers = new ArrayList<TriggerBean>();

        for(BuildStrategy trigger : plan.getTriggers())
        {
            triggers.add(build(trigger));
        }

        return triggers;
    }

    private TriggerBean build(BuildStrategy trigger)
    {
        TriggerAdapter adapter = resolveAdapter(trigger);
        return adapter.getDefinition(trigger);
    }

    public TriggerData getData(Chain plan, TriggerBean trigger)
    {
        TriggerAdapter adapter = resolveAdapter(trigger);
        return adapter.getData(plan, trigger);
    }

    private TriggerAdapter resolveAdapter(BuildStrategy trigger)
    {
        return resolveAdapter(trigger.getKey());
    }

    private TriggerAdapter resolveAdapter(TriggerBean trigger)
    {
        return resolveAdapter(trigger.Type);
    }

    private TriggerAdapter resolveAdapter(String type)
    {
        if(PollingTriggerAdapter.supports(type))
            return new PollingTriggerAdapter(repositoryService);

        throw new UnsupportedOperationException(String.format("Trigger of type: %s is not yet supported", type));
    }
}

