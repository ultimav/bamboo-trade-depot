package com.carolynvs.bamboo.plugin.trade_depot.triggers;

import com.atlassian.bamboo.plan.PlanKey;
import org.apache.commons.configuration.HierarchicalConfiguration;

import java.util.HashSet;

public class TriggerData
{
    public PlanKey Plan;
    public String Description;
    public HashSet<Long> Repositories = new HashSet<Long>();
    public HierarchicalConfiguration Configuration = new HierarchicalConfiguration();
}
