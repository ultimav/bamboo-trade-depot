package it.com.carolynvs.bamboo.plugin.trade_depot;

import com.atlassian.plugins.osgi.test.AtlassianPluginsTestRunner;
import com.carolynvs.bamboo.plugin.trade_depot.*;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

@RunWith(AtlassianPluginsTestRunner.class)
public class PlanImportWiredTest
{
    private final BuildExporter exporter;
    private BuildImporter importer;
    private int nextProject = 1;
    private int nextPlan = 1;

    public PlanImportWiredTest(BuildExporter exporter, BuildImporter importer)
    {
        this.exporter = exporter;
        this.importer = importer;

        initialize();
    }

    private void initialize()
    {
        importer.deleteProjects();
        waitForDeletionThread();
    }

    @Test
    public void importProjectWithPlan()
            throws Exception
    {
        ProjectBean project = createProjectWithPlan();

        ProjectBean result = importer.importProject(project);

        assertNotNull(result);
        assertProjectExists(project.Key);
        assertPlanExists(project.Key, project.Plans.get(0).Key);
    }

    @Test
    public void deleteProject()
            throws Exception
    {
        ProjectBean project = createProjectWithPlan();
        importer.importProject(project);

        importer.deleteProject(project.Key);

        assertProjectDoesNotExist(project.Key);
        assertPlanDoesNotExist(project.Key, project.Plans.get(0).Key);
    }

    @Test
    public void deletePlan()
            throws Exception
    {
        ProjectBean project = createProjectWithPlan();
        String planKey = project.Plans.get(0).Key;
        importer.importProject(project);

        importer.deletePlan(project.Key, planKey);

        assertProjectExists(project.Key);
        assertPlanDoesNotExist(project.Key, planKey);
    }

    private ProjectBean createProjectWithPlan()
    {
        int projectId = nextProject++;
        int planId = nextPlan++;

        ProjectBean project = new ProjectBean();
        project.Key = String.format("PRJ%s", projectId);
        project.Name = String.format("My Project - %s", projectId);

        PlanBean plan = new PlanBean();
        project.Plans.add(plan);
        plan.Key = String.format("PLN%s", planId);
        plan.Name = String.format("My Plan - %s", planId);

        StageBean stage1 = new StageBean();
        stage1.Name = "My Stage 1";
        plan.Stages.add(stage1);

        JobBean job1 = new JobBean();
        stage1.Jobs.add(job1);
        job1.Key = "MJOB1";
        job1.Name = "My Job 1";

        TaskBean task1 = new TaskBean();
        job1.Tasks.add(task1);
        task1.Description = "Hello World";
        task1.Type = "com.atlassian.bamboo.plugins.scripttask:task.builder.script";
        task1.Configuration.put("argument", "");
        task1.Configuration.put("scriptBody", "echo \"Hello World!\"");
        task1.Configuration.put("runWithPowershell", "");
        task1.Configuration.put("workingSubDirectory", "");
        task1.Configuration.put("script", "");
        task1.Configuration.put("environmentVariables", "");
        task1.Configuration.put("scriptLocation", "INLINE");

        return project;
    }

    private void assertProjectExists(String projectKey)
    {
        ProjectBean project = exporter.exportProject(projectKey);
        assertNotNull(project);
    }

    private void assertProjectDoesNotExist(String projectKey)
    {
        waitForDeletionThread();
        ProjectBean project = exporter.exportProject(projectKey);
        assertNull(project);
    }

    private void assertPlanExists(String projectKey, String planKey)
    {
        PlanBean plan = exporter.exportPlan(projectKey, planKey);
        assertNotNull(plan);
    }

    private void assertPlanDoesNotExist(String projectKey, String planKey)
    {
        waitForDeletionThread();
        PlanBean plan = exporter.exportPlan(projectKey, planKey);
        assertNull(plan);
    }

    private void waitForDeletionThread()
    {
        try
        {
            Thread.sleep(1000);
        } catch (InterruptedException e) {}
    }
}